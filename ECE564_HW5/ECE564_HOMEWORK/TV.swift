//
//  TV.swift
//  ECE564_HOMEWORK
//
//  Created by student on 9/26/19.
//  Copyright © 2019 ece564. All rights reserved.
//

import UIKit

class TV: UIView {
    override class var layerClass : AnyClass {
    return Closetlayer.self
    }
    class Closetlayer : CALayer, CALayerDelegate {
    var closetback : CALayer?
    
    override func layoutSublayers() {
    self.setup()
    }
    
    func setup () {
    let closetback = CALayer()
    // Paint the background in colset part
    closetback.contentsScale = UIScreen.main.scale
    closetback.frame = self.bounds
    /// set brown color
    let brownback = UIColor(red: 0.8, green: 0.7, blue: 0, alpha: 1.0)
    closetback.backgroundColor = brownback.cgColor
    let brownedge = UIColor(red: 0.9, green: 0.7, blue: 0, alpha: 1.0)
    closetback.borderColor = brownedge.cgColor
    closetback.borderWidth = 10
    
    self.addSublayer(closetback)
    }
    
    }
   
}
