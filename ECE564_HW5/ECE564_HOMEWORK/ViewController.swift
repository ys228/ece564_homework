//
//  ViewController.swift
//  ECE564_HOMEWORK
//
//  Created by Ric Telford on 7/16/17.
//  Copyright © 2018 ece564. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    

    // create mainview Information
    var Information: UIView!
    var firstNameView: UITextField!     //firstName, 单行文本输入
    var firstNameLabel: UILabel!
    var lastNameView: UITextField!      //lastName, 单行文本输入
    var lastNameLabel: UILabel!

    var gendertextView: UITextField!
    var genderLabel: UILabel!
    var genderpickerView: UIPickerView!      //gender, 选择
    var genderpicker : [Gender] = [Gender.Female,Gender.Male]
    
    var rolepickerView: UIPickerView!       //role, 选择
    var roleLabel: UILabel!
    var roletextView: UITextField!
    var rolepicker: [String] = ["Professor","Teaching Assistant","Student"]
    
    var fromView: UITextField!          //from, 单行文本输入
    var fromLabel: UILabel!
    
    var degreetextView: UITextField!
    var degreeLabel: UILabel!
    var degreepickerView: UIPickerView!      //gender, 选择
    var degreepicker: [Degree]  = [Degree.Associate, Degree.Bachelor, Degree.Master, Degree.Doctoral,Degree.Undefined]
    
    var hobbiesView: UITextField!        //hobbies, 多行文本输入（暂定）
    var hobbiesLabel: UILabel!
    var programmingView: UITextField!    //programming languages, 多行文本输入（暂定）
    var programmingLabel: UILabel!
    
    var teamtextView : UITextField!
    var teamLabel: UILabel!
    
    
    //buttons
    var addUpdate: UIButton!           //add/update, button
    var find: UIButton!           //find, button
    
    //message box
    var descriptionView: UITextView!    //description
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // pickerView functions, which try to select the rows, counts and which row to select
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == genderpickerView{
            return genderpicker.count}
        else if pickerView == rolepickerView{
            return rolepicker.count}
        else if pickerView == degreepickerView{
            return degreepicker.count
        }
        return 0
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == genderpickerView {
            return "\(genderpicker[row])"
        } else if pickerView == rolepickerView{
            return "\(rolepicker[row])"
        }
        else if pickerView == degreepickerView{
            return "\(degreepicker[row])"
        }
        return ""
    }
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == genderpickerView {
            self.gendertextView.text = "\(self.genderpicker[row])"
            genderpickerView.resignFirstResponder()
        } else if pickerView == rolepickerView {
            self.roletextView.text = "\(self.rolepicker[row])"
            rolepickerView.resignFirstResponder()
        } else if pickerView == degreepickerView {
            self.degreetextView.text = "\(self.degreepicker[row])"
            degreepickerView.resignFirstResponder()
        }
    }
    // determine show which pickerview
    func textFieldShouldBeginEditing(_ pickerView: UIPickerView,textField: UITextField) -> Bool {
        if gendertextView == genderpickerView {
        }
        else if pickerView == rolepickerView{
        }
        else if pickerView == degreepickerView{
        }
        return false
    }
    @objc func handleTap(){
        Information.endEditing(true)
    }
    func splithelper(_ textField:UITextField)->[String]{
        let sqstring =  textField.text!.split(separator: ",")
        var stringlist : [String] = [String]()
        for str in sqstring{
            stringlist.append(String(str))
        }
        return stringlist
    }
    // stringarray printout helper
    
    @objc func AddupdateWasPressed(_ sender: UIButton!){
        guard let firN = firstNameView.text, let lastN = lastNameView.text else{
            descriptionView.text = "you didn't enter entire name!"
            return
            
        }
        if  firN.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty || lastN.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty{
            descriptionView.text = "you didn't enter entire name!"
            return
        }
        let name = firN + " " + lastN
        var tempimg = String()
//         wants to add new person to database
        if whoIs(name) == "Cannot find this person. "{
            //sperate the hobbies
            let hobbies = splithelper(hobbiesView)
            let languages = splithelper(programmingView)
            // Edit student to TA
            let newone = DukePerson(hobbies: hobbies, proficiency: languages, role: DukeRole(rawValue: roletextView.text!) ?? .Student , degree: Degree(rawValue: degreetextView.text!) ?? .Undefined, firstName: firstNameView.text!, lastName: lastNameView.text!, whereFrom: fromView.text!, gender: Gender(rawValue: gendertextView.text!) ?? .Male, team: teamtextView.text!, image : Default)
            persondata.append(newone)
            let _ = DukePerson.saveDukePerson(persondata)
//            DukePerson.insertall()
        }
        else {
            var index : Int = 0
            while index < persondata.count{
                
                if firstNameView.text! == persondata[index].firstName && lastNameView.text! == persondata[index].lastName{
                    tempimg = persondata[index].image
                    persondata.remove(at: index)
            let _ = DukePerson.saveDukePerson(persondata)
                        }
                index += 1
                    }
            let hobbies = splithelper(hobbiesView)
            let languages = splithelper(programmingView)
            let newone = DukePerson(hobbies: hobbies, proficiency: languages, role: DukeRole(rawValue: roletextView.text!) ?? .Student , degree: Degree(rawValue: degreetextView.text!) ?? .Undefined, firstName: firstNameView.text!, lastName: lastNameView.text!, whereFrom: fromView.text!, gender: Gender(rawValue: gendertextView.text!) ?? .Male, team: teamtextView.text!,image: tempimg)
            persondata.append(newone)
            let _ = DukePerson.saveDukePerson(persondata)
            descriptionView.text! = "Update Successed"
        }
    }
    

    @objc func FindWasPressed(_ sender: UIButton!){
        guard let firN = firstNameView.text, let lastN = lastNameView.text else{
            descriptionView.text = "you didn't enter entire name!"
            return
            
        }
        if  firN.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty || lastN.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty{
            descriptionView.text = "you didn't enter entire name!"
            return
        }
        let name = firN + " " + lastN
        descriptionView.text = whoIs(name)
        var index : Int = 0
        while index < persondata.count{
            if firstNameView.text! == persondata[index].firstName && lastNameView.text! == persondata[index].lastName{
                fromView.text! = persondata[index].whereFrom
                degreetextView.text! = "\(persondata[index].degree)"
                gendertextView.text! = "\(persondata[index].gender)"
                roletextView.text! = "\(persondata[index].role)"
                let hobbystr  = strarrhelper(persondata[index].hobbies)
                hobbiesView.text! = hobbystr
                let langstr  = strarrhelper(persondata[index].proficiency)
                programmingView.text! = langstr
                teamtextView.text! = persondata[index].team
            }
            index += 1
        }
    
        return
    }
    
    private func FontandColor(_ field: inout UITextField){
        field.font = .boldSystemFont(ofSize: 18)
        field.backgroundColor = .lightGray
    }
    func showCellPerson(thisperson : DukePerson){
        firstNameView.text! = thisperson.firstName
        lastNameView.text! = thisperson.lastName
        fromView.text! = thisperson.whereFrom
        degreetextView.text! = "\(thisperson.degree)"
        gendertextView.text! = "\(thisperson.gender)"
        roletextView.text! = "\(thisperson.role)"
        let hobbystr  = strarrhelper(thisperson.hobbies)
        hobbiesView.text! = hobbystr
        let langstr  = strarrhelper(thisperson.proficiency)
        programmingView.text! = langstr
        teamtextView.text! = thisperson.team
        
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        Information = self.view            //mainView, 表示主界面
//        self.persondata = DukePerson.insertall()
        let textfieldaxis = (Information.bounds.width - 200) / 2+40
        
        
        //firstname initail part
        firstNameLabel = UILabel()
        firstNameView = UITextField(frame: CGRect(x: textfieldaxis, y: 80, width: 240, height: 30))
        firstNameLabel.frame = CGRect(x: 20, y: 80, width: 100, height: 30)
        firstNameLabel.text = "First Name"
        firstNameView.delegate = self
        firstNameView.placeholder = "Enter here"
        FontandColor(&firstNameView)
        Information.addSubview(firstNameView)
        Information.addSubview(firstNameLabel)
        
        //lastname initial part
        lastNameLabel = UILabel()
        lastNameView = UITextField(frame: CGRect(x: textfieldaxis, y: 115, width: 240, height: 30))
        lastNameLabel.frame = CGRect(x: 20, y: 115, width: 100, height: 30)
        lastNameLabel.text = "Last Name"
        lastNameView.delegate = self
        lastNameView.placeholder = "Enter last name"
        FontandColor(&lastNameView)
        Information.addSubview(lastNameView)
        Information.addSubview(lastNameLabel)
        
        //wherefrom initial part
        fromLabel = UILabel()
        fromView = UITextField(frame:CGRect(x: textfieldaxis, y: 150, width: 240, height: 30))
        fromLabel.frame = CGRect(x: 20, y: 150, width: 100, height: 30)
        fromLabel.text = "Where From"
        fromView.delegate = self
        fromView.placeholder = "where from."
        FontandColor(&fromView)
        Information.addSubview(fromView)
        Information.addSubview(fromLabel)
        
        // genderpickerview and gendertextview settings
        genderLabel = UILabel()
        genderpickerView = UIPickerView()
        //hidden before user select this textfield
        self.genderpickerView.delegate = self
        gendertextView = UITextField(frame: CGRect(x: textfieldaxis, y: 185, width: 240, height: 30))
        genderLabel.frame = CGRect(x: 20, y: 185, width: 100, height: 30)
        genderLabel.text = "Gender"
        gendertextView.placeholder = "gender"
        gendertextView.inputView = genderpickerView
        FontandColor(&gendertextView)
        Information.addSubview(gendertextView)
        Information.addSubview(genderLabel)
        
        
        // rolepickerview and roletextview settings
        roleLabel = UILabel()
        rolepickerView = UIPickerView()
        self.rolepickerView.dataSource = self
        self.rolepickerView.delegate = self
        roletextView = UITextField(frame: CGRect(x: textfieldaxis, y: 220, width: 240, height: 30))
        roleLabel.frame = CGRect(x: 20, y: 220, width: 100, height: 30)
        roleLabel.text = "Duke Role"
        roletextView.inputView = rolepickerView
        roletextView.placeholder = "role"
        FontandColor(&roletextView)
        Information.addSubview(roletextView)
        Information.addSubview(roleLabel)
        
        
        // rolepickerview and roletextview settings
        degreeLabel = UILabel()
        degreepickerView = UIPickerView()
        self.degreepickerView.dataSource = self
        self.degreepickerView.delegate = self
        degreetextView = UITextField(frame: CGRect(x: textfieldaxis, y: 255, width: 240, height: 30))
        degreeLabel.frame = CGRect(x: 20, y: 255, width: 100, height: 30)
        degreeLabel.text = "Degree"
        degreetextView.inputView = degreepickerView
        degreetextView.placeholder = "degree"
        FontandColor(&degreetextView)
        Information.addSubview(degreetextView)
        Information.addSubview(degreeLabel)
        
        //hobbies initial  part
        hobbiesLabel = UILabel()
        hobbiesView = UITextField(frame:CGRect(x: textfieldaxis, y: 290, width: 240, height: 30))
        hobbiesLabel.frame = CGRect(x: 20, y: 290, width: 100, height: 30)
        hobbiesLabel.text = "Hobbies"
        hobbiesView.delegate = self
        hobbiesView.placeholder = "Use ',' to seperate"
        FontandColor(&hobbiesView)
        Information.addSubview(hobbiesView)
        Information.addSubview(hobbiesLabel)
        
        //languages initial part
        programmingLabel = UILabel()
        programmingView = UITextField(frame:CGRect(x: textfieldaxis, y: 325, width: 240, height: 30))
        programmingLabel.frame = CGRect(x: 20, y: 325, width: 100, height: 30)
        programmingLabel.text = "Proficiency"
        programmingView.delegate = self
        programmingView.placeholder = "Use ',' to seperate"
        FontandColor(&programmingView)
        Information.addSubview(programmingView)
        Information.addSubview(programmingLabel)
        
        teamLabel = UILabel()
        teamtextView = UITextField(frame:CGRect(x: textfieldaxis, y: 360, width: 240, height: 30))
        teamLabel.frame = CGRect(x: 20, y: 360, width: 100, height: 30)
        teamLabel.text = "Team"
        teamtextView.delegate = self
        teamtextView.placeholder = "TeamName"
        FontandColor(&teamtextView)
        
        
        Information.addSubview(teamtextView)
        Information.addSubview(teamLabel)
        
        
        addUpdate = UIButton()
        addUpdate.frame = CGRect(x: textfieldaxis-110, y: 395, width: 150, height: 30)
        addUpdate.setTitle("Add/Update", for: .normal)
        addUpdate.backgroundColor = .orange
        addUpdate.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        addUpdate.addTarget(self, action: #selector(AddupdateWasPressed), for: .touchUpInside)
        self.view.addSubview(addUpdate)
        
        find = UIButton()
        find.frame = CGRect(x: textfieldaxis+80, y: 395, width: 150, height: 30)
        find.setTitle("Find", for: .normal)
        find.backgroundColor = .blue
        find.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        find.addTarget(self, action: #selector(FindWasPressed), for: .touchUpInside)
        Information.addSubview(find)
        
        descriptionView = UITextView()
        descriptionView.frame = CGRect(x: textfieldaxis-110, y: 430, width: 340, height: 80)
        descriptionView.backgroundColor = .green
        descriptionView.isUserInteractionEnabled = false
        Information.addSubview(descriptionView)
        
        
        
      
//        let image: UIImage = #imageLiteral(resourceName: "Mypic")
        let image: UIImage = decodeimage(imagecode: SylImage)

        let imageView = UIImageView(image: image)
        imageView.frame = CGRect(x: textfieldaxis-10, y: 520, width: 100, height: 100)
        imageView.contentMode = .scaleAspectFill
        self.view.addSubview(imageView)
        //Imageview on Top of View
        self.view.bringSubviewToFront(imageView)
        
        let tap = UITapGestureRecognizer(target: Information, action: #selector(UIView.endEditing))
        tap.cancelsTouchesInView = false
        Information.addGestureRecognizer(tap)
    }
//    func  AddupdateWasPressed

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

