//
//  TableViewController.swift
//  ECE564_HOMEWORK
//
//  Created by student on 9/17/19.
//  Copyright © 2019 ece564. All rights reserved.
//

import UIKit

@available(iOS 11.0, *)
class TableViewController: UITableViewController, UISearchBarDelegate{
    //initial the original(lastest vesrion) of persondata list
    
    @IBOutlet weak var Search: UISearchBar!
    
    var Result:[DukePerson] = []

    func loadInitialData() {
        if let tempList = DukePerson.loadDukePerson()  {
            persondata = tempList
            print("loaded successed")
            print(persondata)
        } else {
            let item1 = DukePerson(hobbies:["Golf", "Swimming", "Reading"],proficiency:["Swift", "C","C++"],role: DukeRole.Professor,degree: Degree.Bachelor, firstName :"Ric",lastName:"Telford",whereFrom:"Chatham County, NC",gender: Gender.Male,team:"",image: Default)
            let item2 = DukePerson(hobbies: ["Road Cycling","Swimming","Reading"], proficiency: ["Swift","Python","C++","Matlab"], role: DukeRole.TA, degree: Degree.Undefined, firstName: "Ting", lastName: "Chen", whereFrom: "Beijing, China", gender: Gender.Male,team:"", image: Default)
            let item3 = DukePerson(hobbies: ["Reading","Jogging"], proficiency: ["Swift","Python"], role: DukeRole.TA, degree: Degree.Master, firstName: "Haohong", lastName: "Zhao", whereFrom: "Heibei, China", gender: Gender.Male,team:"", image: Default)
            let item4 = DukePerson(hobbies: ["Watching TV series","Cooking","Working out"], proficiency: ["Matlab","Python","C++"], role: DukeRole.Student, degree: Degree.Bachelor, firstName: "Yilun", lastName: "Sun", whereFrom: "Hubei, China", gender: Gender.Male, team:"+1s",image : SylImage)
            let item5 = DukePerson(hobbies: ["Linguistics", "Eating out", "Bodybuidling"], proficiency: ["Python", "JAVA", "C"], role: DukeRole.Student, degree: Degree.Bachelor, firstName: "Yijia", lastName: "Hu", whereFrom: "Xi'an, China", gender: Gender.Male, team:"+1s", image: HyjImage)
            let item6 = DukePerson(hobbies : ["eatting", "reading", "sleeping"], proficiency: ["C++", "Swift", "Java"], role: DukeRole.Student, degree: Degree.Bachelor, firstName: "Xiaochen", lastName: "Zhou", whereFrom: "Hubei, China", gender: Gender.Female, team:"+1s", image: ZxcImage)
            
            let item7 = DukePerson(hobbies : ["sleeping", "cooking", "reading"], proficiency: ["C++", "Java", "Spark"], role: DukeRole.Student, degree: Degree.Bachelor, firstName: "Fang", lastName: "Zhang", whereFrom: "Tianjing, China", gender: Gender.Female, team:"+1s", image: ZfImage)
            
            persondata.append(item1)
            persondata.append(item2)
            persondata.append(item3)
            persondata.append(item4)
            persondata.append(item5)
            persondata.append(item6)
            persondata.append(item7)
            
            
            print("loaded failed, inital ")
            
            let _ = DukePerson.saveDukePerson(persondata)
        }
        init2Darray()
    }
    func init2Darray(){
        //
        twoDarray = []
        listTeamName(Persondata: persondata)
        profess = findsameRoles(Persondata: persondata, PersonRole: DukeRole.Professor)
//        students = findsameRoles(Persondata: persondata, PersonRole: DukeRole.Student)
        TAs = findsameRoles(Persondata: persondata, PersonRole: DukeRole.TA)
//        twoDarray = [profess,TAs,students]
        twoDarray = [profess,TAs]
        for name in AllteamName {
            thisTeam = findsameTeam(Persondata: persondata, PersonTeam: name)
        twoDarray.append(thisTeam)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Search.delegate = self
        self.tableView.delegate = self
        self.tableView.dataSource = self
        loadInitialData()
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)

        if searchText == "" {
            init2Darray()
        }
        else { //
            self.Result = []
            for people in persondata {
                let peoplename = people.firstName + " " + people.lastName
                if peoplename.lowercased().hasPrefix(searchText.lowercased()) {
                    twoDarray = []
                    twoDarray.append([people])
                }
            }
        }

        self.tableView.reloadData()
    }
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool{
        return true
        
    }
    
    
    override func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String?{
        return "Delete"
    }
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath)
    {
        
        if editingStyle == UITableViewCell.EditingStyle.delete {
            let target = twoDarray[indexPath.section][indexPath.row]
            let index = findperson(first: target.firstName, last: target.lastName)
            print("before remove ", persondata[index].firstName)
            persondata.remove(at: index)
            let _ = DukePerson.saveDukePerson(persondata)
            
            print("remove finished")
            //刷新tableview
            init2Darray()
            //tableView.beginUpdates()
            if(indexPath.row == 0){
            tableView.deleteSections([indexPath.section], with: UITableView.RowAnimation.automatic)
        }
        else{
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            
        }
            //tableView.endUpdates()

        }
    }
   
   
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel()
        label.backgroundColor = .lightGray
        label.font = UIFont(name: "Times New Roman", size: 19.0)
        if section < 2 {
        //        if section < 3{
        
        switch section {
        case 0:
            label.text  = "Professor"
        case 1:
            label.text = "TAs"
//        case 2:
//            label.text = "Student"
        
        default:
            label.text = ""
            }
        }
        else{
            if AllteamName[section-2] != "" {
                label.text = AllteamName[section-2]
            }
            else {
                label.text = "Team Undefined"
            }
//            print(AllteamName[section-3])
//            if AllteamName[section-3] != "" {
//            label.text = AllteamName[section-3]
//            }
//            else {
//                label.text = "Team Undefined"
//            }
        }
        
        return label
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return twoDarray.count
    }


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return twoDarray[section].count
        
    }
    
    @IBAction func unwindTableView(segue: UIStoryboardSegue){
//        Here need to change for re-load twoD again !!!!!
        init2Darray()
//        profess = findsameRoles(Persondata: persondata, PersonRole: DukeRole.Professor)
//            students = findsameRoles(Persondata: persondata, PersonRole: DukeRole.Student)
//            TAs = findsameRoles(Persondata: persondata, PersonRole: DukeRole.TA)
//            twoDarray = [profess,TAs]
            self.tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // call 2D array to display all cells in different sections
        let person = twoDarray[indexPath.section][indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "PersonInfo") as! PersonInfo
        cell.setCell(person: person)
        
        return cell
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CelldetailSegues"{
            let showupInfo = segue.destination as! ShowCellViewController
            var selectedindexPath = tableView.indexPathForSelectedRow
            let selectOne = twoDarray[(selectedindexPath?.section)!][(selectedindexPath?.row)!]
            showupInfo.thisperson = selectOne
            passtemp = selectOne
            print(passtemp.firstName)
        }

    }

}
