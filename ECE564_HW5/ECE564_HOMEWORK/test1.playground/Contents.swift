import UIKit
//
//  test.swift
//  ECE564_HOMEWORK
//
//  Created by student on 10/3/19.
//  Copyright © 2019 ece564. All rights reserved.
//

enum Gender : String, Codable{
    case Male = "Male"
    case Female = "Female"
}

//class Person : Codable{
//    var firstName :String = "First"
//    var lastName :String = "Last"
//    var whereFrom :String = "Anywhere"  // this is just a free String - can be city, state, both, etc.
//    var gender : Gender = .Male

// here we don't need codingkeys

//}
protocol BlueDevil {
    var hobbies : [String] { get }
    var role : DukeRole { get }
}

enum DukeRole : String, Codable {
    case Student = "Student"
    case Professor = "Professor"
    case TA = "Teaching Assistant"
}

enum Degree: String, Codable{
    case Associate = "Associte"
    case Bachelor = "Bachelor's"
    case Master = "Master's"
    case Doctoral = "Doctoral"
    case Undefined = "Undefined"
}
// delete Person parent class
// here I add NSOject and delete CustomStringConvertible
class DukePerson:NSObject, BlueDevil,Codable{
    var firstName :String = "First"
    var lastName :String = "Last"
    var whereFrom :String = "Anywhere"  // this is just a free String - can be city, state, both, etc.
    var gender : Gender = .Male
    var hobbies: [String]
    var proficiency: [String]
    var role: DukeRole
    var degree: Degree
    var team: String
    
    /// the third person is determined by gender
    
    func gend()->String{
        var trdperson : String
        switch self.gender {
        case .Female:
            trdperson = "She "
        default:
            trdperson = "He "
        }
        return trdperson
    }
    
    /// proficiency description
    
    func prof()->String{
        var proficient = ""
        /// if he(she) doesn't have proficient languages, then doesn't print anything about languages
        if proficiency.isEmpty{
            
        }
        else{
            var pproficiency :[String]
            proficient = gend() + "is proficient in "
            /// only print out first three
            if proficiency.count > 3{
                pproficiency = Array(proficiency.prefix(3))
            }
            else{
                pproficiency = proficiency
            }
            proficient += pproficiency.first!
            for language in pproficiency.dropFirst(){
                if language == pproficiency.last{
                    proficient +=  " and "
                }
                else{
                    proficient += ", "
                }
                proficient += language
                
            }
            proficient += ". "
        }
        return proficient
    }
    
    
    func hobb()->String{
        var hobby = ""
        /// hobbies description is below
        var hhobby : [String]
        if hobbies.count > 3{
            hhobby = Array(hobbies.prefix(3))
        }
        else{
            hhobby = hobbies
        }
        if hhobby.isEmpty{
        }
        else{
            hobby += "When not in class, \(firstName) enjoys "
            hobby += hhobby.first!
            for thishobby in hhobby.dropFirst(){
                if thishobby == hhobby.last{
                    hobby += " and "
                }
                else{
                    hobby += ", "
                }
                hobby += thishobby
            }
        }
        return hobby
    }
    
    
    func degr()->String{
        var degreedescri = ""
        if degree != Degree.Undefined{
            degreedescri = " \(gend())has a \(degree) degree. "
        }
        else{
            degreedescri = "Can't find degree information. "
        }
        return degreedescri
    }
    
    
    override var description: String {
        /// the description here conduct how to print person info
        
        return "\(firstName) \(lastName) is from \(whereFrom) and is a \(role)." +  degr() + prof() + hobb() + "."
    }
    
    
    
    init(hobbies :[String], proficiency: [String], role: DukeRole,degree: Degree,firstName: String,lastName: String, whereFrom : String, gender: Gender,team: String) {
        self.hobbies = hobbies
        self.proficiency  = proficiency
        self.role = role
        self.degree = degree
        self.team = team
        //        super.init()
        self.firstName = firstName
        self.lastName = lastName
        self.whereFrom = whereFrom
        self.gender = gender
    }
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("DukePersonFile")
    
    // save DukePerson to FILE
    static func saveDukePerson(_ items: [DukePerson]) -> Bool {
        var outputData = Data()
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(items) {
            if let json = String(data: encoded, encoding: .utf8) {
                print(json)
                outputData = encoded
            }
            else { return false }
            
            do {
                try outputData.write(to: ArchiveURL)
            } catch let error as NSError {
                print (error,"\n")
                return false
            }
            return true
        }
        else { return false }
    }
    // Load DukePerson from FILE
    static func loadDukePerson() -> [DukePerson]? {
        let decoder = JSONDecoder()
        var loadedData = [DukePerson]()
        let tempData: Data
        
        do {
            tempData = try Data(contentsOf: ArchiveURL)
        } catch let error as NSError {
            print(error,"\n")
            return nil
        }
        if let decoded = try? decoder.decode([DukePerson].self, from: tempData) {
            print(decoded[1].description)
            loadedData = decoded
        }
        return loadedData
    }
    
}

// Haven't added TEAM YET
func whoIs(_ name: String)-> String{
    // separate firstname and lastname
    var names = name.split(separator: " ")
    let firstName = names[0]
    let lastName = names[1]
    var index : Int
    var samepos = false
    var sameposp : [String] = []
    var samecount = 0
    var sameperc : Double
    index = 0
    //print how many people currently we have in database
    print("Right now we have \(persondata.count) in database\n")
    while index < persondata.count{
        if firstName == persondata[index].firstName && lastName == persondata[index].lastName{
            for people in 0..<persondata.count{
                // for people who has the same role as searching target, record their name and count
                if persondata[people].role == persondata[index].role && firstName != persondata[people].firstName && lastName != persondata[people].lastName{
                    samepos = true
                    let temp = persondata[people].firstName + " " + persondata[people].lastName
                    sameposp.append(temp)
                    samecount += 1
                }
            }
            // if the symbol is true,which means there exist other person with same role, print following info
            if samepos == true{
                sameperc = (Double(samecount+1)/Double(persondata.count)*100).rounded()
                print("There are \(sameperc)% people in dataset have the same role\n")
                print("Try to search:")
                for j in sameposp{
                    print(j)
                }
                print("\n")
            }
            return persondata[index].description;
        }
        index += 1
    }
    return "Cannot find this person. "
}
func findsameRoles( Persondata: [DukePerson],PersonRole: DukeRole)->[DukePerson]{
    var index = 0
    var sameRole :[DukePerson] = []
    while index < Persondata.count{
        // for people who has the same role as searching target, record their name and count
        if Persondata[index].role == PersonRole {
            sameRole.append(Persondata[index])
        }
        index += 1
    }
    return sameRole
}
func strarrhelper(_ arr : [String])->String{
    var str = ""
    var helper = [String]()
    /// hobbies description is below
    if arr.count > 3{
        helper = Array(arr.prefix(3))
    }
    else{
        helper = arr
    }
    if helper.isEmpty{
    }
    else{
        str += helper.first!
        for this in helper.dropFirst(){
            if this == helper.last{
                str += ","
            }
            else{
                str += ","
            }
            str += this
        }
    }
    return str
}
var persondata = [ DukePerson(hobbies:["Golf", "Swimming", "Reading"],proficiency:["Swift", "C","C++"],role: DukeRole.Professor,degree: Degree.Bachelor, firstName :"Ric",lastName:"Telford",whereFrom:"Chatham County, NC",gender: Gender.Male,team:""),
                   DukePerson(hobbies: ["Road Cycling","Swimming","Reading"], proficiency: ["Swift","Python","C++","Matlab"], role: DukeRole.TA, degree: Degree.Undefined, firstName: "Ting", lastName: "Chen", whereFrom: "Beijing, China", gender: Gender.Male,team:""),
                   DukePerson(hobbies: ["Reading","Jogging"], proficiency: ["Swift","Python"], role: DukeRole.TA, degree: Degree.Master, firstName: "Haohong", lastName: "Zhao", whereFrom: "Heibei, China", gender: Gender.Male,team:""),
                   DukePerson(hobbies: ["Watching TV series","Cooking","Working out"], proficiency: ["Matlab","Python","C++"], role: DukeRole.Student, degree: Degree.Bachelor, firstName: "Yilun", lastName: "Sun", whereFrom: "Hubei, China", gender: Gender.Male, team:"Team")]

var profess = findsameRoles(Persondata: persondata, PersonRole: DukeRole.Professor)
var students = findsameRoles(Persondata: persondata, PersonRole: DukeRole.Student)
var TAs = findsameRoles(Persondata: persondata, PersonRole: DukeRole.TA)
var twoDarray = [profess,TAs,students]
var passtemp :DukePerson!


func test(){
    var data = [DukePerson]()
    if let temp = DukePerson.loadDukePerson() {
        data = temp
        print(data[0].description)
        let item1  = DukePerson(hobbies: ["1"], proficiency: ["item1"], role: DukeRole.TA, degree: Degree.Undefined, firstName: "1", lastName: "1", whereFrom: "item1", gender: Gender.Male,team:"")
        data.append(item1)
        let _ = DukePerson.saveDukePerson(data)
        let afterchange = DukePerson.loadDukePerson()
        print(afterchange?[0].description ?? 0)
    } else {
        let item1  = DukePerson(hobbies: ["0"], proficiency: ["item1"], role: DukeRole.TA, degree: Degree.Undefined, firstName: "1", lastName: "0", whereFrom: "0", gender: Gender.Male,team:"")
        data.append(item1)
        
        
    }
}

test()
