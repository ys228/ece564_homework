//
//  HobbyViewController.swift
//  ECE564_HOMEWORK
//
//  Created by student on 9/23/19.
//  Copyright © 2019 ece564. All rights reserved.
//

import UIKit
import AVFoundation
import Foundation

var audioPlayer: AVAudioPlayer?

class HobbyViewController: UIViewController {
    let TVview = TV()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assignbackground()
        setTV()
        setTVcontroller()
        setButton()
        MusicPlayer.shared.startBackgroundMusic()
        
    }
    func setTV() {
        TVview.frame = CGRect(x: view.bounds.width/2-160, y: view.bounds.height/2-90, width: 320, height: 180)
        TVview.layer.shadowPath =
            UIBezierPath(roundedRect: TVview.bounds,
                         cornerRadius: TVview.layer.cornerRadius).cgPath
        TVview.layer.shadowColor = UIColor.gray.cgColor
        TVview.layer.shadowOpacity = 0.5
        TVview.layer.shadowOffset = CGSize(width: 10, height: 10)
        TVview.layer.shadowRadius = 1
        TVview.layer.masksToBounds = false
        self.view.addSubview(TVview)

}
    // draw a TV controller at the corner
    func setTVcontroller(){
        let gcv2Frame = CGRect(x: 20, y: 450, width: 60, height: 110)
        
        UIGraphicsBeginImageContextWithOptions(gcv2Frame.size, false, 0.0)
        // bpath is TV controller bpath2 is play button
        let color:UIColor = UIColor.black
        let bpath:UIBezierPath = UIBezierPath()
        bpath.move(to: CGPoint(x: 30, y: 0))
        bpath.addLine(to: CGPoint(x: 60, y: 0))
        bpath.addLine(to: CGPoint(x: 45, y: 110))
        bpath.addLine(to: CGPoint(x: 0, y: 110))
        bpath.close()
        bpath.lineWidth = 2
        color.set()
        bpath.fill()
        let color2:UIColor = UIColor.red
        let bpath2:UIBezierPath = UIBezierPath()
        bpath2.move(to: CGPoint(x: 52, y: 30))
        bpath2.addLine(to: CGPoint(x: 31, y: 20))
        bpath2.addLine(to: CGPoint(x: 26, y: 40))
        bpath2.close()
        color2.set()
        bpath2.lineWidth = 2
        bpath2.fill()
        let saveImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        let iv = UIImageView()
        iv.frame = gcv2Frame
        iv.image = saveImage
        
        view.addSubview(iv)
    }
    func assignbackground(){
        let background = UIImage(named: "BackGround.png")
        
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIView.ContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubviewToBack(imageView)
    }
    func setImageAnimationView(xlen:Int, ylen:Int, wlen:Int, hlen: Int) {
        let iav = UIImageView()
        iav.frame = CGRect(x: xlen, y: ylen, width: wlen, height: hlen)
        
        let i1 = UIImage(named: "AHS1984_1.png")!
        let i2 = UIImage(named: "AHS1984_2.png")!
        let i3 = UIImage(named: "AHS1984_3.png")!
        let i4 = UIImage(named: "AHS1984_4.png")!
        let i5 = UIImage(named: "AHS1984_5.png")!
        iav.animationImages = [i1, i2, i3, i4, i5]
        iav.animationDuration = 2
        iav.startAnimating()
        view?.addSubview(iav)
        
    }
    func setButton(){
        let play : UIButton = UIButton(type: .custom)
        let buttonplay = UIImage(named: "ButtonPlay.png")
        play.setImage(buttonplay,for: .normal)
        play.frame = CGRect(x: 39, y: 505, width: 25, height: 25)
        play.layer.cornerRadius = 0.5 * play.bounds.size.width
        play.addTarget(self, action: #selector(playwaspressed), for: .touchUpInside)
        play.clipsToBounds = true
        let music : UIButton = UIButton(type: .custom)
        let buttonmusic = UIImage(named: "Stop.png")
        music.setImage(buttonmusic,for: .normal)
        music.frame = CGRect(x: 320, y: 110, width: 25, height: 25)
        music.layer.cornerRadius = 0.5 * play.bounds.size.width
        music.addTarget(self, action: #selector(musicwaspressed), for: .touchUpInside)
        music.clipsToBounds = true
        view.addSubview(play)
        view.addSubview(music)

    }
    
    @objc func playwaspressed(_ sender: UIButton!){
        setImageAnimationView(xlen: Int(view.bounds.width/2-150), ylen: Int(view.bounds.height/2-80), wlen: Int(TVview.bounds.width-20), hlen: Int(TVview.bounds.height-20))
        return
            
        }
    
    @objc func musicwaspressed(_ sender: UIButton!){
        audioPlayer?.volume = 0
    }
}


class MusicPlayer {
    static let shared = MusicPlayer()
    
    func startBackgroundMusic() {
        if let bundle = Bundle.main.path(forResource: "American Horror Story", ofType: "mp3") {
            let backgroundMusic = NSURL(fileURLWithPath: bundle)
            do {
                audioPlayer = try AVAudioPlayer(contentsOf:backgroundMusic as URL)
                print("its playing!")
                guard let audioPlayer = audioPlayer else { return }
                audioPlayer.numberOfLoops = -1
                audioPlayer.prepareToPlay()
                audioPlayer.play()
            } catch {
                print(error)
            }
        }
    }
    func stopMusic() {
        print("notification observed - music stopped")
    }
}
