//
//  PersonInfo.swift
//  ECE564_HOMEWORK
//
//  Created by student on 9/18/19.
//  Copyright © 2019 ece564. All rights reserved.
//

import UIKit

class PersonInfo: UITableViewCell {

    @IBOutlet weak var CellDescription: UITextView!
    @IBOutlet weak var CellLabel: UILabel!

    @IBOutlet weak var CellImage: UIImageView!
    func setCell(person: DukePerson){
        CellLabel.text = person.firstName + " " + person.lastName
        CellDescription.text = person.description
        let afterdecodeImg = decodeimage(imagecode: person.image)
        CellImage.image = afterdecodeImg
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
