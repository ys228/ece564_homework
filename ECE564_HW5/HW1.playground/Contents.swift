import UIKit

/*:
 ### ECE 564 HW1 assignment - Please see ECE 564 Syllabus and Handbook for complete information on what is required!
 In this first assignment, you are going to create a base data model for storing information about the students, TAs and Professor in ECE 564. You need to populate your data model with at least 4 records, but can add more.  You will also provide a search function ("whoIs") to search an array of those objects by first name and last name.
 I suggest you create a new class called `DukePerson` and have it subclass `Person`.  You also need to abide by 2 protocols:
 1. BlueDevil
 2. CustomStringConvertible
 
 You also need to follow good OO practices, such as containing any properties and methods that deal with `DukePerson` within the class definition.
 */

/*:
 Below is an example of what the string output from `whoIs' should look like:
 
 Ric Telford is from Chatham County, NC and is a Professor. He is proficient in Swift, C and C++. When not in class, Ric enjoys Golf, Swimming and Reading.
 */

/*:
 ### Refer to the ECE 564 Syllabus and Handbook for complete Functional Requirements for this Homework!
 */

enum Gender {
    case Male
    case Female
}

class Person {
    var firstName :String = "First"
    var lastName :String = "Last"
    var whereFrom :String = "Anywhere"  // this is just a free String - can be city, state, both, etc.
    var gender : Gender = .Male
    
}

enum DukeRole : String {
    case Student = "Student"
    case Professor = "Professor"
    case TA = "Teaching Assistant"
}

protocol BlueDevil {
    var hobbies : [String] { get }
    var role : DukeRole { get }
}
//: ### START OF HOMEWORK
//: Do not change anything above.
//: Put your code here:
/// set several options for degree parameter
enum Degree: String{
    case Associate = "Associte"
    case Bachelor = "Bachelor's"
    case Master = "Master's"
    case Doctoral = "Doctoral"
    case Undefined = "Undefined"
}
/// DukePerson class follows BuleDevil and CustomStringConvertible and it's Person's subclass
class DukePerson: Person, BlueDevil,CustomStringConvertible{
    var hobbies: [String]
    var proficiency: [String]
    var role: DukeRole
    var degree: Degree
    /// the third person is determined by gender
    func gend()->String{
        var trdperson : String
        switch self.gender {
        case .Female:
            trdperson = "She "
        default:
            trdperson = "He "
        }
        return trdperson
    }
    /// proficiency description
    func prof()->String{
        var proficient = ""
        /// if he(she) doesn't have proficient languages, then doesn't print anything about languages
        if proficiency.isEmpty{
            
        }
        else{
            var pproficiency :[String]
            proficient = gend() + "is proficient in "
            /// only print out first three
            if proficiency.count > 3{
                pproficiency = Array(proficiency.prefix(3))
            }
            else{
                pproficiency = proficiency
            }
            proficient += pproficiency.first!
            for language in pproficiency.dropFirst(){
                if language == pproficiency.last{
                    proficient +=  " and "
                }
                else{
                    proficient += ", "
                }
                proficient += language
                
            }
            proficient += ". "
        }
        return proficient
    }
    func hobb()->String{
        var hobby = ""
        /// hobbies description is below
        var hhobby : [String]
        if hobbies.count > 3{
            hhobby = Array(hobbies.prefix(3))
        }
        else{
            hhobby = hobbies
        }
        if hhobby.isEmpty{
        }
        else{
            hobby += "When not in class, \(firstName) enjoys "
            hobby += hhobby.first!
            for thishobby in hhobby.dropFirst(){
                if thishobby == hhobby.last{
                    hobby += " and "
                }
                else{
                    hobby += ", "
                }
                hobby += thishobby
            }
        }
        return hobby
    }
    func degr()->String{
        var degreedescri = ""
        if degree != Degree.Undefined{
            degreedescri = " \(gend())has a \(degree) degree. "
        }
        else{
            degreedescri = "Can't find degree information. "
        }
        return degreedescri
    }
    var description: String {
        /// the description here conduct how to print person info
        
        return "\(firstName) \(lastName) is from \(whereFrom) and is a \(role)." +  degr() + prof() + hobb() + "."
    }
    init(hobbies :[String], proficiency: [String], role: DukeRole,degree: Degree,firstName: String,lastName: String, whereFrom : String, gender: Gender) {
        self.hobbies = hobbies
        self.proficiency  = proficiency
        self.role = role
        self.degree = degree
        super.init()
        self.firstName = firstName
        self.lastName = lastName
        self.whereFrom = whereFrom
        self.gender = gender
    }
}

func whoIs(_ name: String)-> String{
    // separate firstname and lastname
    var names = name.split(separator: " ")
    let firstName = names[0]
    let lastName = names[1]
    var index : Int
    var samepos = false
    var sameposp : [String] = []
    var samecount = 0
    var sameperc : Double
    index = 0
    //print how many people currently we have in database
    print("Right now we have \(persondata.count) in database\n")
    while index < persondata.count{
        if firstName == persondata[index].firstName && lastName == persondata[index].lastName{
            for people in 0..<persondata.count{
                // for people who has the same role as searching target, record their name and count
                if persondata[people].role == persondata[index].role && firstName != persondata[people].firstName && lastName != persondata[people].lastName{
                    samepos = true
                    let temp = persondata[people].firstName + " " + persondata[people].lastName
                    sameposp.append(temp)
                    samecount += 1
                }
            }
            // if the symbol is true,which means there exist other person with same role, print following info
            if samepos == true{
                sameperc = (Double(samecount+1)/Double(persondata.count)*100).rounded()
                print("There are \(sameperc)% people in dataset have the same role\n")
                print("Try to search:")
                for j in sameposp{
                    print(j)
                }
                print("\n")
            }
            return persondata[index].description;
        }
        index += 1
    }
    return "Cannot find this person. ";
}
// main code starts here
var persondata: [DukePerson]
let person1 = DukePerson(hobbies:["Golf", "Swimming", "Reading"],proficiency:["Swift", "C","C++"],role: DukeRole.Professor,degree: Degree.Bachelor, firstName :"Ric",lastName:"Telford",whereFrom:"Chatham County, NC",gender: Gender.Male)
let person2 = DukePerson(hobbies: ["Road Cycling","Swimming","Reading"], proficiency: ["Swift","Python","C++","Matlab"], role: DukeRole.TA, degree: Degree.Undefined, firstName: "Ting", lastName: "Chen", whereFrom: "Beijing, China", gender: Gender.Male)
let person3 = DukePerson(hobbies: ["Reading","Jogging"], proficiency: ["Swift","Python"], role: DukeRole.TA, degree: Degree.Master, firstName: "Haohong", lastName: "Zhao", whereFrom: "Heibei, China", gender: Gender.Male)
let person4 = DukePerson(hobbies: ["watching TV series","cooking","working out"], proficiency: ["Matlab","Python","C++"], role: DukeRole.Student, degree: Degree.Bachelor, firstName: "Yilun", lastName: "Sun", whereFrom: "Hubei, China", gender: Gender.Male)
persondata = [person1,person2,person3,person4]

//: ### END OF HOMEWORK
//: Uncomment the line below to test your homework.
//: The "whoIs" function should be defined as `func whoIs(_ name: String) -> String {   }`
print(whoIs("Ric Telford"))
///test other person
//print(whoIs("Ting Chen"))
///test non-exist person
//print(whoIs("Ting Chan"))
