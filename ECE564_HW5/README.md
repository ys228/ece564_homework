This is your README file - you need to add any info for each homework that you want to make sure we grade - like extra function.
### HW1 -  
1. I created a data model to store Professor, TA and my Information. I used array to store each person’s information. 
2. I created a function called whoIs which allows the database to be searched and print out relative information
3. The require “signature” of whoIs is “func whoIs(_ name: String)-> String”
4. I created a class called DukePerson which is the subclass of Person. DukePerson has properties such as hobbies, proficiency, role, degree, description. Description here is to set the right format to print out. DukePerson follows protocols: BlueDevil and CustomStringConvertible.
5. Follow the good OO practices: use switch/case in description to determine the third person(He/She)
6. Additional Include: Each person’s degree, up to 3 programming languages
7. Something MUST include:  (1) array, class, function, enum(2) string, int, double, bool(3) for/in while if/else switch/case (4) arithmetic(+=), comparison(<, !=), range(..<), logical(&&)
8. Extra function:
(1). Print out current number of people in database like below:
Right now we have 4 in database
(2). If the name user enter cannot match with names in database, it will print out “Can’t find this person”
(3).  If there are other people in database who has the same role as target person, it will print out extra lines below:
There are …% people in dataset have the same role

Try to search:

(People’s Name)

### HW2 - 
1. Created a mainview called Information, which includes following textfields and label: First Name, Last Name, Gender, Role, From, Hobbies, Programming Languages. 
2. Created a UItextView to display the output string
3. Set two buttons on the screen - "Add/Update" and "Find", Add/Update is used for add a new person or update someone's data, find button is used for find someone's data.
4. put a UIview as a placeholder for picture



Extra Functions:
(1) Used Picker control for Gender, Role and Degree
(2) Added placeholder for each textfield to hint user
(3) Error Checking: if user donesn't enter firstname or lastname(including entering only blanks), the textfield below will print out "you didn't enter entire name"
(4) Added ImageView to hold picture
(5) Used different color and font
(6) When user find exact person's data, the textfield will autofill with related information 
(7) The layout looks tidy and clean
(8) Dimiss the keyboard when user click somewhere expcect the keyboard place
### HW3
• Pre-populate a table view called TableView and show each person's data. Use Section to seperate different roles.

• Add a “+” button at the top-right corner to present the Information View to add/find/Update Person information, when user return to table view page, the data will displayed as the lasted data.

• Create a cell type for displaying the “Person” information. Present Person's Name and Description

• When user celect any cell in the table, switch to Person Infomation View to display each character he/she has. This view is not editable

• User can choose to edit present person's data, press the edit button on top-right, show the editable view. 

• Press the Save on the top-right to keep Change and press Return on top-left to return to table View

• The functions you removed do not go away – they are moved as follows:



Extra Function
Add Team to DukePerson class, Team textfield and label to Information View.
Different Font and Text size for each cell
### HW4
• On the Show Perosnal Information view, add a button “Flip” to my "Hobbies" view


• When user click the filp button, the View will be replaced by a screen-sized Drawing you created. Click the return button to filp it back


• The Hobbies page shows a television which displays tv series.

• This hobbies page contains UIImages(the button image and animations) the television is drawed by Beziered path and save as an image. So did the TV controller

Thereis also an animation on the TV

EXTRA 

o Push the button on TV controller to display the animation

o Added background music, push the button on the top-left to stop it


o Added UIimage as a UIview background 

### HW5
• use JSONEncoder to save data


• Section title displays student Team name, if student doesn't have team name, put it into team undefined. TA and Professor don't have team.


• On the Table View, drag the cell to right can delete target cell. When the whole section is empty, delete the whole cell


• The search bar is on the top of view, Enter name came display target cell.


• Display each personal Image on the tableview and celldisplay view.


Extra :


Implement Other team members' infomation.

